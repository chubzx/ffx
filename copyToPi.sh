#!/bin/bash

PI_HOST='192.168.1.164'
PI_USER='nick'
PI_PATH='/home/nick/scripts'

echo 'Copying Ligntning Dodger to Pi'
echo "scp ./lightningDodger.py $PI_USER@$PI:/$PI_PATH"
scp ./lightningDodger.py $PI_USER@$PI_HOST:/$PI_PATH
echo 'Copy Complete'

echo 'Update Permissions'
echo "ssh $PI_USER@$PI_HOST chmod +x $PI_PATH/lightningDodger.py"
ssh $PI_USER@$PI_HOST chmod +x $PI_PATH/lightningDodger.py